# Letitbit (скачать без регистрации и смс)

### Production

```bash
$ docker-compose up
```

Then go to http://localhost:3000 

### Development

#### Run backend development

```bash
$ ./server-dev.sh
```

Inside the container:

```bash
$ npm i # install all npm dependencies
$ npm run debug # run server in develop mode with logging
```

#### Run frontend development

```bash
$ ./client-dev.sh
```

Inside the container:

```bash
$ npm i # install all npm dependencies
$ npm start # start webpack dev server on 9080 port
```

Link to frontend dev server: http://localhost:9080
