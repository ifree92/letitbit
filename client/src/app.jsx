// Basic styles
import './app.scss';
// React
import React from 'react';
import ReactDOM from 'react-dom';
// Redux
import { createStore } from 'redux';
import { Provider } from 'react-redux';
import reducers from './state/reducers';
// Material UI
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
// Pages
import MainPage from './pages/MainPage';

const store = createStore(reducers);

// Entry component
const App = function App() {
  return (
    <Provider store={store}>
      <MuiThemeProvider>
        <MainPage />
      </MuiThemeProvider>
    </Provider>
  );
};

// Render root component
ReactDOM.render(<App />, document.getElementById('app-container'));
