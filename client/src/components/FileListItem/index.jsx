// React
import React, { Component } from 'react';
import PropTypes from 'prop-types';
// Material UI
import { ListItem } from 'material-ui/List';
import Avatar from 'material-ui/Avatar';
import CloudDownloadIcon from 'material-ui/svg-icons/file/cloud-download';
import { blue500 } from 'material-ui/styles/colors';
// helpers
import { normalizeSize, getFormattedDateTime } from '../../helpers/utils';

const propTypes = {
  name: PropTypes.string,
  uploaded: PropTypes.string,
  expiration: PropTypes.string,
  size: PropTypes.number,
  onClick: PropTypes.func,
};

const defaultProps = {
  name: 'unnamed file',
  uploaded: 'unknown date',
  expiration: 'unknown date',
  size: -1,
  onClick: () => {},
};

class FileListItem extends Component {
  constructor(props) {
    super(props);
    this.onClick = this.onClick.bind(this);
  }

  onClick() {
    this.props.onClick(this.props.name);
  }

  render() {
    const { name, expiration, size } = this.props;
    const fixedFileName = name.length > 30 ? `${name.substr(0, 30)}...` : name;
    return (
      <ListItem
        leftAvatar={<Avatar icon={<CloudDownloadIcon />} backgroundColor={blue500} />}
        primaryText={`${fixedFileName} (${normalizeSize(size)})`}
        secondaryText={`Expiration: ${getFormattedDateTime(new Date(expiration))}`}
        onClick={this.onClick}
      />
    );
  }
}

FileListItem.propTypes = propTypes;
FileListItem.defaultProps = defaultProps;

export default FileListItem;
