// React
import React from 'react';
import PropTypes from 'prop-types';
// Material UI
import { List, ListItem } from 'material-ui/List';
import Subheader from 'material-ui/Subheader';
// Components
import FileListItem from '../FileListItem';

const propTypes = {
  files: PropTypes.array,
  onItemClick: PropTypes.func,
};

const defaultProps = {
  files: [],
  onItemClick: () => {},
};

function FilesList({ files, onItemClick }) {
  return (
    <div>
      <List>
        <Subheader inset={true}>Files</Subheader>
        {files.map(file => (
          <FileListItem
            key={file.name}
            name={file.name}
            uploaded={file.uploaded}
            expiration={file.expiration}
            size={file.size}
            onClick={onItemClick}
          />
        ))}
      </List>
    </div>
  );
}

FilesList.propTypes = propTypes;
FilesList.defaultProps = defaultProps;

export default FilesList;
