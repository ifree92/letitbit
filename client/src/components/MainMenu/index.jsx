// React
import React, { Component } from 'react';
import PropTypes from 'prop-types';
// Material UI
import IconButton from 'material-ui/IconButton';
import MoreVertIcon from 'material-ui/svg-icons/navigation/more-vert';
import IconMenu from 'material-ui/IconMenu';
import MenuItem from 'material-ui/MenuItem';
import { white } from 'material-ui/styles/colors';
import FileUploadIcon from 'material-ui/svg-icons/file/file-upload';
import RefreshIcon from 'material-ui/svg-icons/navigation/refresh';

const propTypes = {
  onActionClick: PropTypes.func,
};

const defaultProps = {
  onActionClick: () => {},
};

export const menuActions = {
  uploadFile: 'UPLOAD_FILE',
  refreshPage: 'REFRESH_PAGE',
};

class MainMenu extends Component {
  constructor(props) {
    super(props);
    this.onItemClick = this.onItemClick.bind(this);
  }

  onItemClick(event, child) {
    const { onActionClick } = this.props;
    const { action } = child.props;
    onActionClick(action);
  }

  render() {
    return (
      <IconMenu
        iconButtonElement={<IconButton><MoreVertIcon color={white} /></IconButton>}
        targetOrigin={{ horizontal: 'right', vertical: 'top' }}
        anchorOrigin={{ horizontal: 'right', vertical: 'top' }}
        onItemClick={this.onItemClick}
      >
        <MenuItem action={menuActions.uploadFile} primaryText="Upload" leftIcon={<FileUploadIcon />} />
        <MenuItem action={menuActions.refreshPage} primaryText="Refresh" leftIcon={<RefreshIcon />} />
      </IconMenu>
    );
  }
}

MainMenu.propTypes = propTypes;
MainMenu.defaultProps = defaultProps;

export default MainMenu;

