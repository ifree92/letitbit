// React
import React, { Component } from 'react';
import PropTypes from 'prop-types';
// Material UI
import Dialog from 'material-ui/Dialog';
import FlatButton from 'material-ui/FlatButton';
import RaisedButton from 'material-ui/RaisedButton';
import LinearProgress from 'material-ui/LinearProgress';
import DatePickerDialog from 'material-ui/DatePicker/DatePickerDialog';
import TimePickerDialog from 'material-ui/TimePicker/TimePickerDialog';
import DateTimePicker from 'material-ui-datetimepicker';
import Snackbar from 'material-ui/Snackbar';


const propTypes = {
  onAction: PropTypes.func,
  open: PropTypes.bool,
  uploadProgress: PropTypes.number,
  uploadProcess: PropTypes.bool,
};

const defaultProps = {
  onAction: () => {},
  open: false,
  uploadProgress: 0,
  uploadProcess: false,
};

export const uploadDialogActions = {
  upload: 'UPLOAD',
  cancel: 'CANCEL',
};

class UploadDialog extends Component {
  constructor(props) {
    super(props);
    this.state = {
      file: null,
      selectedDate: null,
      showAlert: false,
    };
    this.handleCancel = this.handleCancel.bind(this);
    this.handleUpload = this.handleUpload.bind(this);
    this.onFileSelected = this.onFileSelected.bind(this);
    this.clearChosenFile = this.clearChosenFile.bind(this);
    this.handleSelectedDateTime = this.handleSelectedDateTime.bind(this);
  }

  componentWillUpdate() {
    if (!this.props.open && this.state.file) {
      this.clearChosenFile();
    }
  }

  onFileSelected(elem) {
    this.setState({
      file: elem.target.files[0],
    });
  }

  clearChosenFile() {
    this.setState({
      file: null,
      selectedDate: null,
      showAlert: false,
    });
  }

  handleCancel() {
    this.props.onAction(uploadDialogActions.cancel);
  }

  handleUpload() {
    this.props.onAction(uploadDialogActions.upload, {
      file: this.state.file,
      expiration: this.state.selectedDate,
    });
  }

  handleSelectedDateTime(dateTime) {
    if (!dateTime) {
      return this.setState({ selectedDate: null });
    }
    const expirationDateTime = dateTime.getTime();
    const cur = Date.now();
    if (expirationDateTime <= cur) {
      this.setState({ showAlert: true, selectedDate: null });
    } else {
      this.setState({ showAlert: false, selectedDate: expirationDateTime });
    }
  }

  render() {
    const { uploadProgress, uploadProcess } = this.props;

    const actions = [
      <FlatButton
        label="Cancel"
        primary={true}
        onClick={this.handleCancel}
        disabled={uploadProcess}
      />,
      <FlatButton
        label="Upload"
        primary={true}
        disabled={!this.state.file || !this.state.selectedDate}
        onClick={this.handleUpload}
      />,
    ];

    return (
      <div>
        <Dialog
          title="Dialog With Actions"
          actions={actions}
          modal={true}
          open={this.props.open}
        >
          <div
            style={{
              display: 'flex',
              flexDirection: 'column',
            }}
          >
            <DateTimePicker
              className="date-time-picker"
              returnMomentDate={false}
              DatePicker={DatePickerDialog}
              TimePicker={TimePickerDialog}
              floatingLabelText="Set expiration date"
              format="MMM DD, YYYY hh:mm A"
              timePickerDelay={150}
              value={this.state.selectedDate}
              onChange={this.handleSelectedDateTime}
              disabled={uploadProcess}
            />
            <RaisedButton
              containerElement="label"
              label={this.state.file ? this.state.file.name : 'Choose file...'}
              disabled={uploadProcess}
            >
              <input
                type="file"
                style={{ display: 'none' }}
                onChange={this.onFileSelected}
                disabled={uploadProcess}
              />
            </RaisedButton>
            { uploadProcess ? <LinearProgress mode="determinate" value={uploadProgress} /> : '' }
          </div>
        </Dialog>
        <Snackbar
          open={this.state.showAlert}
          message="Expiration date is incorrect"
          autoHideDuration={4000}
        />
      </div>
    );
  }
}

UploadDialog.propTypes = propTypes;
UploadDialog.defaultProps = defaultProps;

export default UploadDialog;
