import axios from 'axios';

class Api {
  constructor(apiHost) {
    this.host = apiHost;
  }

  getFiles() {
    return axios.get(`${this.host}/files`);
  }

  getFile(file) {
    const a = document.createElement('a');
    a.href = `${this.host}/files/${file}`;
    a.target = '_blank';
    a.click();
  }

  uploadFile({ file, expiration }, cbProgress) {
    const formData = new FormData();
    formData.append('file', file);
    formData.append('expiration', expiration);
    return axios.post(`${this.host}/files/`, formData, {
      onUploadProgress: (progressEvent) => {
        if (typeof cbProgress === 'function') {
          cbProgress(Math.round((progressEvent.loaded * 100) / progressEvent.total));
        }
      },
    });
  }
}

export default new Api(__API_HOST || '');
