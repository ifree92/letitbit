/* eslint-disable no-console */

const isDev = () => __ENV === 'dev';

class Log {
  constructor(label) {
    this.label = label;
  }

  silly(...vars) {
    const preLog = `[s][${this.label}]`;
    Log._log(preLog, ...vars);
  }

  s(...vars) {
    this.silly(...vars);
  }

  debug(...vars) {
    const preLog = `[d][${this.label}]`;
    Log._log(preLog, ...vars);
  }

  d(...vars) {
    this.debug(...vars);
  }

  verbose(...vars) {
    const preLog = `[v][${this.label}]`;
    Log._log(preLog, ...vars);
  }

  v(...vars) {
    this.verbose(...vars);
  }

  info(...vars) {
    const preLog = `[i][${this.label}]`;
    Log._log(preLog, ...vars);
  }

  i(...vars) {
    this.info(...vars);
  }

  warn(...vars) {
    const preLog = `[w][${this.label}]`;
    console.warn(preLog, ...vars);
  }

  w(...vars) {
    this.warn(...vars);
  }

  error(...vars) {
    const preLog = `[e][${this.label}]`;
    console.error(preLog, ...vars);
  }

  e(...vars) {
    this.error(...vars);
  }

  static _log(...vars) {
    if (isDev()) {
      console.log(...vars);
    }
  }
}

const logger = label => new Log(label);

export default logger;
/* eslint-enable no-console */
