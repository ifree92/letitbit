export const normalizeSize = (size) => {
  let _size = parseInt(size, 10);
  if (!Number.isFinite(_size)) return '0';
  if (_size < 1024) return `${_size}b`;
  else {
    _size /= 1024;
    if (_size < 1024) return `${_size.toFixed(2)}kb`;
    else {
      _size /= 1024;
      if (_size < 1024) return `${_size.toFixed(2)}mb`;
      else {
        _size /= 1024;
        if (_size < 1024) return `${_size.toFixed(2)}gb`;
        else {
          _size /= 1024;
          if (_size < 1024) return `${_size.toFixed(2)}tb`;
        }
      }
    }
  }
};

export const getFormattedDateTime = (date) => {
  if (!(date instanceof Date)) return '';
  return `${date.toLocaleDateString()} ${date.toLocaleTimeString()}`;
};

export const isDev = () => __ENV === 'dev';

export default {};
