// React
import React, { Component } from 'react';
import PropTypes from 'prop-types';
// Material UI
import AppBar from 'material-ui/AppBar';
import LinearProgress from 'material-ui/LinearProgress';
// Redux
import { connect } from 'react-redux';
import actions from '../../state/actions';
// Components
import MainMenu, { menuActions } from '../../components/MainMenu';
import FilesList from '../../components/FilesList';
import UploadDialog, { uploadDialogActions } from '../../components/UploadDialog';
// Api
import Api from '../../core/api';
// Log
import logger from '../../helpers/log';
const log = logger('MainPage');

const propTypes = {
  files: PropTypes.array,
  setFiles: PropTypes.func,
};

const defaultProps = {
  files: [],
  setFiles: () => {},
};

const mapToStateProps = state => ({
  files: state.files,
});

const mapDispatchToProps = dispatch => ({
  setFiles: data => dispatch(actions.files.set(data)),
});

class MainPage extends Component {
  static downloadFile(file) {
    Api.getFile(file);
  }

  constructor(props) {
    super(props);
    this.state = {
      process: false,
      openUploadDialog: false,
      uploadProcess: false,
      uploadProgress: 0,
    };
    this.onMenuAction = this.onMenuAction.bind(this);
    this.refreshFiles = this.refreshFiles.bind(this);
    this.onUploadDialogAction = this.onUploadDialogAction.bind(this);
  }

  componentWillMount() {
    this.refreshFiles();
  }

  onMenuAction(action) {
    switch (action) {
    case menuActions.refreshPage:
      this.refreshFiles();
      break;
    case menuActions.uploadFile:
      this.setState({ openUploadDialog: true });
      break;
    default:
    }
  }

  onUploadDialogAction(action, dataFile) {
    switch (action) {
    case uploadDialogActions.cancel:
      this.setState({ openUploadDialog: false });
      break;
    case uploadDialogActions.upload:
      log.d('upload file', dataFile);
      this.setState({ uploadProcess: true });
      Api.uploadFile(dataFile, (progress) => {
        log.d('upload progress', progress);
        this.setState({ uploadProgress: progress });
      }).then((response) => {
        log.d(response.data);
        this.setState({ uploadProcess: false, uploadProgress: 0, openUploadDialog: false });
        this.refreshFiles();
      }).catch((err) => {
        this.setState({ uploadProcess: false, uploadProgress: 0, openUploadDialog: false });
        log.w('Unable upload file:', err);
      });
      break;
    default:
    }
  }

  refreshFiles() {
    const { setFiles } = this.props;
    this.setState({ process: true });
    Api.getFiles()
      .then((response) => {
        const { data } = response;
        setFiles(data);
        this.setState({ process: false });
      })
      .catch((err) => {
        log.w('Unable to get files list:', err);
        this.setState({ process: false });
      });
  }

  render() {
    const { files } = this.props;
    const { process } = this.state;

    return (
      <div>
        {process ? <LinearProgress mode="indeterminate" /> : ''}
        <AppBar
          title="Letitbit"
          iconElementRight={<MainMenu onActionClick={this.onMenuAction} />}
        />
        <UploadDialog
          open={this.state.openUploadDialog}
          onAction={this.onUploadDialogAction}
          uploadProcess={this.state.uploadProcess}
          uploadProgress={this.state.uploadProgress}
        />
        <FilesList
          files={files}
          onItemClick={MainPage.downloadFile}
        />
      </div>
    );
  }
}

MainPage.propTypes = propTypes;
MainPage.defaultProps = defaultProps;

export default connect(mapToStateProps, mapDispatchToProps)(MainPage);
