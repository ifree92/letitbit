export const types = {
  SET_FILES: 'SET_FILES',
};

export default {
  files: {
    set: data => ({ type: types.SET_FILES, data }),
  },
};
