import { types } from './actions';

function files(state = [], action) {
  switch (action.type) {
  case types.SET_FILES:
    return action.data;
  default:
    return state;
  }
}

export default function mainReducer(state = {}, action) {
  return {
    files: files(state.files, action),
  };
}
