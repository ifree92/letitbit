// Require
const path = require('path');
const webpack = require('webpack');
// Package
const pack = require('./package.json');
// Plugins
const HtmlWebpackPlugin = require('html-webpack-plugin');
const CopyWebpackPlugin = require('copy-webpack-plugin');
// Paths
const distPath = path.resolve(__dirname, 'dist');
const appPath = path.resolve(__dirname, 'src');

// Config
const config = {
  entry: {
    app: `${appPath}/app.jsx`,
  },
  output: {
    path: distPath,
    publicPath: '/',
    filename: 'js/[name].js',
  },
  resolve: {
    alias: {
      assets: `${appPath}/assets`,
      components: `${appPath}/components`,
      core: `${appPath}/core`,
      helpers: `${appPath}/helpers`,
      pages: `${appPath}/pages`,
      services: `${appPath}/services`,
      state: `${appPath}/state`,
      style: `${appPath}/style`,
      containers: `${appPath}/containers`,
    },
    extensions: ['.js', '.jsx'],
  },
  module: {
    loaders: [
      { test: /\.jsx?/, use: 'babel-loader', include: appPath },
      { test: /\.(png|woff|woff2|eot|ttf|svg)/, use: 'url-loader?limit=100000' },
      { test: /\.css$/, use: ['style-loader', 'css-loader'] },
      { test: /\.scss$/, use: ['style-loader', 'css-loader', 'sass-loader'] },
    ],
  },
  plugins: [
    new HtmlWebpackPlugin({
      filename: 'index.html',
      template: 'src/templates/index.html',
      hash: true,
      minify: {
        collapseWhitespace: true,
        removeComments: true,
        minifyJS: true,
      },
    }),
    new CopyWebpackPlugin([
      { context: 'src', from: 'assets/img/**' },
      { context: 'src', from: 'assets/fonts/**' },
    ]),
    new webpack.DefinePlugin({
      __VERSION: JSON.stringify(pack.version),
      __ENV: JSON.stringify(process.env.ENV || 'prod'),
      __API_HOST: JSON.stringify(process.env.API_HOST || ''),
    }),
  ],
  devServer: {
    port: 9080,
    historyApiFallback: true,
    host: '0.0.0.0',
  },
  devtool: 'source-map',
};

module.exports = config;
