const log = require('./helpers/log')(module);
const path = require('path');
const http = require('http');

const express = require('express');
const bodyParser = require('body-parser');
const fileUpload = require('express-fileupload');
const mongoose = require('mongoose');

const { mongo } = require('./config');
const { normalizePort, isDev } = require('./helpers/utils');

// Routes
const routerFiles = require('./routes/files');

mongoose.connect(`mongodb://${mongo.host}:${mongo.port}/${mongo.storage}`, { useMongoClient: true }, (err) => {
  if (err) return log.error('Unable connect to Mongo:', err);
  log.info('MongoDB Connected');
});

const port = normalizePort(process.env.PORT) || 3000;
const app = express();

// express app config
if (isDev()) app.use(require('./helpers/cors')());
app.use(express.static(path.join(__dirname, 'public')));
app.set('view engine', 'html');
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(fileUpload());

// requests logging
app.use((req, res, next) => {
  log.debug(req.method, req.originalUrl, req.body);
  next();
});

// express routes
app.use('/files', routerFiles);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  const err = new Error('Not Found');
  err.status = 404;
  next(err);
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};
  res.status(err.status || 500).send({desc: err.message || 'internal server error'});
});

app.set('port', port);
const server = http.createServer(app);
server.listen(port);

server.on('error', (err) => {
  if (error.syscall !== 'listen') {
    throw error;
  }
  const bind = typeof port === 'string'
    ? 'Pipe ' + port
    : 'Port ' + port;

  // handle specific listen errors with friendly messages
  switch (error.code) {
    case 'EACCES':
      log.error(bind + ' requires elevated privileges');
      process.exit(1);
      break;
    case 'EADDRINUSE':
      log.error(bind + ' is already in use');
      process.exit(1);
      break;
    default:
      throw error;
  }
});

server.on('listening', () => {
  const addr = server.address();
  const bind = typeof addr === 'string'
    ? 'pipe ' + addr
    : 'port ' + addr.port;
  log.info(`Listening on ${bind}`);
});