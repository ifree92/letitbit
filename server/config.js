const path = require('path');

module.exports = {
  storageFolder: path.resolve(__dirname, '..', 'storage'),
  mongo: {
    host: 'mongo',
    port: 27017,
    storage: 'letitbit',
  },
};