const log = require('../helpers/log')(module);
const path = require('path');
const config = require('../config');
const { checkOrCreateFolder, checkAndRemove } = require('../helpers/disk');
const FileModel = require('../models/FileModel');
const fileLocker = require('../helpers/file-locker');

module.exports.getAllFiles = (req, res, next) => {
  FileModel.getAll()
  .then((files) => {
    res.send(files.filter((file) => {
      const curDate = new Date();
      const expirationDate = new Date(file.expiration);
      if (curDate >= expirationDate) {
        checkAndRemove(file);
        return false;
      }
      return true;
    }));
  })
  .catch(err => res.status(500).send({ desc: err.message }));
};

module.exports.getFile = (req, res, next) => {
  const fileName = req.params.file;
  const file = path.resolve(config.storageFolder, fileName);
  FileModel.get(fileName)
    .then((fileInfo) => {
      if (!fileInfo) return res.status(404).send({ desc: 'the file not found on the server' });
      let curDate = new Date();
      const expirationDate = new Date(fileInfo.expiration);
      if (curDate >= expirationDate) {
        res.status(404).send({ desc: 'the file not found on the server' });
        checkAndRemove(fileInfo);
      } else {
        fileLocker.lock(fileName);
        res.download(file, (err) => {
          if (err) return res.status(404).send({desc: 'file not found'});
          fileLocker.unlock(fileName);
          checkAndRemove(fileInfo);
        });
      }
    })
    .catch((err) => {
      res.status(500).send({ desc: err.message });
    });
};

module.exports.saveFile = (req, res, next) => {
  const { expiration } = req.body;
  const size = req.headers['content-length'];
  if (!expiration || !size) return resolve.status(400).send({desc: '"expiration" or "size" is incorrect'});
  if ('file' in req.files) {
    const file = req.files.file;
    checkOrCreateFolder(config.storageFolder);
    file.mv(path.resolve(config.storageFolder, file.name), (err) => {
      if (err) return res.status(500).send({ desc: err.message });
      FileModel.add({
        name: file.name,
        uploaded: new Date(),
        expiration: new Date(parseInt(expiration, 10)),
        size
      })
      .then(() => res.send('ok'))
      .catch((err) => res.status(500).send({ desc: err.message }));
    });
  } else {
    res.status(400).send({desc: 'no "file" field found'});
  }
};