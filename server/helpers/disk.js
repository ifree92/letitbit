const log = require('./log')(module);
const fs = require('fs');
const path = require('path');
const config = require('../config');
const fileLocker = require('./file-locker');
const FileModel = require('../models/FileModel');

/**
 * Remove file from storage by name
 * @param {String} file
 */
const remove = (file) => {
  try {
    fs.unlinkSync(path.resolve(config.storageFolder, file));
  } catch (e) {
    log.error(e);
  }
};

module.exports.remove = remove;

/**
 * Check if folder exists and create it if not
 * @param {String} path 
 */
module.exports.checkOrCreateFolder = (path) => {
  if (!fs.existsSync(path)) {
    try {
      fs.mkdirSync(path);
    } catch (e) {}
  }
};

/**
 * Check the file expiration date and remove if it is expired
 * @param {Object} fileInfo
 * @param {String} fileInfo.name
 * @param {Date} fileInfo.expiration
 */
module.exports.checkAndRemove = (fileInfo) => {
  const curDate = new Date();
  const expirationDate = new Date(fileInfo.expiration);
  if (curDate >= expirationDate && !fileLocker.check(fileInfo.name)) {
    FileModel.remove(fileInfo.name)
      .then(() => remove(fileInfo.name))
      .catch((err) => remove(fileInfo.name));
  }
}