class FileLocker {
  constructor() {
    this.files = {};
  }

  lock(file) {
    if (file in this.files) {
      this.files[file]++;
    } else {
      this.files[file] = 1;
    }
  }

  unlock(file) {
    if (file in this.files) {
      this.files[file]--;
      if (this.files[file] === 0) {
        delete this.files[file];
      }
    }
  }

  check(file) {
    return this.files[file] > 0 ? true : false;
  }
}

module.exports = (new FileLocker());
