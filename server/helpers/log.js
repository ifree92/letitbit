const winston = require('winston');
const path = require('path');
const { isDev } = require('../helpers/utils');

const getLabelName = (_module) => typeof _module === 'object' && 'filename' in _module ? path.basename(_module.filename, '.js') : '';

module.exports = (_module) => {
  return new winston.Logger({
    transports: [
      new winston.transports.Console({
        json: false,
        colorize: true,
        level: isDev() ? 'silly' : 'info',
        label: getLabelName(_module),
      }),
    ],
  });
};