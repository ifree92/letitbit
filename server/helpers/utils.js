module.exports.normalizePort = (port) => {
  const _port = parseInt(port, 10);
  return _port > 0 && isFinite(_port) ? _port : false;
};

module.exports.isDev = () => process.env.ENV === 'dev';