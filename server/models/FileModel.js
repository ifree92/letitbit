const log = require('../helpers/log')(module);
const mongoose = require('mongoose');
const FileSchema = require('../schemas/FileSchema');

const File = mongoose.model('File', FileSchema);

/**
 * Add new file to DB
 * @param {Object} file 
 * @param {String} file.name
 * @param {Date} file.uploaded
 * @param {Date} file.expiration
 * @param {Number} file.size
 * @returns {Promise}
 */
module.exports.add = ({name, uploaded, expiration, size}) => {
  log.debug('add', {name, uploaded, expiration, size});
  const file = new File({ name, uploaded, expiration, size });
  return new Promise((resolve, reject) => {
    File.findOne({name}, (err, res) => {
      if (res) {
        File.remove(res, (err) => {
          file.save((err) => {
            if (err) reject(err);
            else resolve();
          });
        });
      } else {
        file.save((err) => {
          if (err) reject(err);
          else resolve();
        });
      }
    });
  });
};

/**
 * Get file by name
 * @param {String} name
 * @returns {Promise} 
 */
module.exports.get = (name) => {
  log.debug('get', name);
  return new Promise((resolve, reject) => {
    File.findOne({name}, (err, res) => {
      if (err) reject(err);
      else resolve(res);
    });
  });
};

/**
 * Get list of files with offset and limit sorted by uploading date
 * @param {Object} options 
 * @param {Number} options.limit
 * @param {Number} options.offset
 * @returns {Promise}
 */
module.exports.getAll = (options = {}) => {
  log.debug('getAll', options);
  return new Promise((resolve, reject) => {
    File.find({})
      .sort({uploaded: -1})
      .skip(Number.isFinite(options.offset) ? options.offset : 0)
      .limit(Number.isFinite(options.limit) ? options.limit : 0)
      .exec((err, res) => {
        if (err) reject(err);
        else resolve(res);
      });
  });
};

/**
 * Remove file entry in DB by name
 * @param {String} name 
 * @returns {Promise}
 */
module.exports.remove = (name) => {
  log.debug('remove', name);
  return new Promise((resolve, reject) => {
    File.remove({name}, (err) => {
      if (err) reject(err);
      else resolve();
    });
  });
};