const { Router } = require('express');
const router = Router();
const filesCtrl = require('../controllers/filesCtrl');

// Get list files
router.get('/', filesCtrl.getAllFiles);
// Get specific file
router.get('/:file', filesCtrl.getFile);
// Save uploaded to server file
router.post('/', filesCtrl.saveFile);

module.exports = router;