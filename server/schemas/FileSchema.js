const mongoose = require('mongoose');

module.exports = new mongoose.Schema({
  name: String,
  uploaded: Date,
  expiration: Date,
  size: Number,
});